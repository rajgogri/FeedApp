Web Services URLs:

Login:
https://27.32.5.149:9884/token
grant_type=password&username=Neosofttech&password=Df@454f926b1F



Upload image bytes to get ID:

https://27.32.5.149:9884/api/fileUpload/ImageUpload

Returns image Guid in:  data.imageGuid

Add new record:
https://27.32.5.149:9884/api/diaryData/addDiaryData

Example json:
{
 	"recID": null,
	"CategoryID" : "00000000-0000-0000-0000-000000000000",  //pass Empty Guid in case required for future use.
	"diaryCategory": "MY PICTURES",
	"Title": "TEST NEW DATA",
	"Description": "TESTING ONLY",
	"diaryDate": "24/07/2020",
	"diaryTime": "09:50:00",
	"picturetempID": "f6d4a2e5-12a6-4595-94b2-dc42c666fd5c",  //Don't use this one, get from imageUpload… 
	"HasImage": true,  //Set to false if no image attached, default.
	"FullAddress": "237 Great N Rd, Five Dock NSW 2046, Australia",
	"Country": "Australia",
	"State": "NSW",
	"PostCode": "2046",
	"Region": "City of Canada Bay Council",  //Add region obtained from Google places if available
	"StreetNumber": "237",
	"StreetName": "Great North Road",
	"City": "Five Dock",
	"diaryLat": "-33.864663",
	"diaryLng": "151.130019”
}  

GetAllPost
api/diaryData/getallrecords/Neosofttech


Google places ID (WAN IP Required for whitelisting):

AIzaSyBKRtnXL_r1KpTwPl022nRMtdBK2QT7VQw