﻿using System;
using System.IO;
using FeedApp.Droid.Dependency;
using FeedApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileService))]
namespace FeedApp.Droid.Dependency
{
    public class FileService : IFileService
    {
        //public string SavePicture(string name, Stream data, string location = "temp")
        //{
        //    var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        //    documentsPath = Path.Combine(documentsPath, "Orders", location);
        //    Directory.CreateDirectory(documentsPath);

        //    string filePath = Path.Combine(documentsPath, name);

        //    byte[] bArray = new byte[data.Length];
        //    using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
        //    {
        //        using (data)
        //        {
        //            data.Read(bArray, 0, (int)data.Length);
        //        }
        //        int length = bArray.Length;
        //        fs.Write(bArray, 0, length);
        //    }
        //    return filePath;
        //}

        public string SaveFile(string fileName, byte[] imageStream)
        {
            string path = null;
            string imageFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Personal), "ProductImages");

            //Check if the folder exist or not
            if (!System.IO.Directory.Exists(imageFolderPath))
            {
                System.IO.Directory.CreateDirectory(imageFolderPath);
            }
            string imagefilePath = System.IO.Path.Combine(imageFolderPath, fileName);

            //Try to write the file bytes to the specified location.
            try
            {
                System.IO.File.WriteAllBytes(imagefilePath, imageStream);
                path = imagefilePath;
            }
            catch (System.Exception e)
            {
                throw e;
            }
            return path;
        }

        public void DeleteDirectory()
        {
            string imageFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Personal), "ProductImages");
            if (System.IO.Directory.Exists(imageFolderPath))
            {
                System.IO.Directory.Delete(imageFolderPath, true);
            }
        }
    }
}
