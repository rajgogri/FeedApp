﻿using System;
using Android.Content;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using FeedApp.Controls;
using FeedApp.Droid.Renderers;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace FeedApp.Droid.Renderers
{
    public class CustomEntryRenderer:EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context) { }
        public static void Init()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                Control.Background = null;
                Control.LongClickable = false;
            }
        }
    }
}
