﻿using System;
using Android.Content;
using Android.Graphics.Drawables;
using FeedApp.Controls;
using FeedApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace FeedApp.Droid.Renderers
{
    public class CustomTimePickerRenderer: TimePickerRenderer
    {
        public CustomTimePickerRenderer(Context context) : base(context) { }

        
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
        {
            base.OnElementChanged(e);
            CustomTimePicker element = Element as CustomTimePicker;
            if (element != null)
            {
                // Control.Text = element.Placeholder;
                Control.SetHintTextColor(global::Android.Graphics.Color.Silver);
                Control.Background = null;
                Control.SetBackgroundDrawable(null);
                var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                layoutParams.SetMargins(0, 0, 0, 0);
                LayoutParameters = layoutParams;
                GradientDrawable gd = new GradientDrawable();
                gd.SetStroke(0, Android.Graphics.Color.Transparent);
                Control.SetBackgroundDrawable(gd);
                Control.LayoutParameters = layoutParams;
                Control.Text.ToString(new System.Globalization.CultureInfo("en"));
                Control.SetPadding(0, 0, 0, 0);
                SetPadding(0, 0, 0, 0);
            }
        }
    }
}
