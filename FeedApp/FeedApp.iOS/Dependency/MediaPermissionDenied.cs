﻿using System;
using FeedApp.iOS.Renderers;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(MediaPermissionDenied))]
namespace FeedApp.iOS.Renderers
{
    public class MediaPermissionDenied
    {
        public MediaPermissionDenied()
        {
        }
        static MediaPermissionDenied pathManager;
        public static MediaPermissionDenied Instance
        {
            get
            {
                if (pathManager == null)
                    pathManager = new MediaPermissionDenied();
                return pathManager;
            }
        }

        public void AllowCameraPermission()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl(UIApplication.OpenSettingsUrlString));
        }
    }
}
