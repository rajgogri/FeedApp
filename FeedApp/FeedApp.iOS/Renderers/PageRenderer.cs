﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ContentPage), typeof(FeedApp.iOS.Renderers.PageRenderer))]
namespace FeedApp.iOS.Renderers
{
    public class PageRenderer : Xamarin.Forms.Platform.iOS.PageRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                //SetAppTheme();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"\t\t\tERROR: {ex.Message}");
            }
        }

        public override void TraitCollectionDidChange(UITraitCollection previousTraitCollection)
        {
            base.TraitCollectionDidChange(previousTraitCollection);
            


        }

        //void SetAppTheme()
        //{
        //    if (this.TraitCollection.UserInterfaceStyle == UIUserInterfaceStyle.Dark)
        //    {
        //        if (App.AppTheme == Theme.Dark)
        //            return;
        //        //Add a Check for App Theme since this is called even when not changed really
        //        App.Current.Resources = new DarkTheme();

        //        App.AppTheme = Theme.Dark;
        //    }
        //    else
        //    {
        //        if (App.AppTheme != Theme.Dark)
        //            return;
        //        App.Current.Resources = new LightTheme();
        //        App.AppTheme = Theme.Light;
        //    }
        //}
    }
}
