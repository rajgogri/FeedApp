﻿using System;
using System.IO;
using FeedApp.Controls;
using FeedApp.LiteDBManager;
using FeedApp.ViewModels;
using FeedApp.Views;
using Plugin.SharedTransitions;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;

namespace FeedApp
{
    [AutoRegisterForNavigation]
    public partial class App
    {
        public static Theme AppTheme { get; set; }
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        static LiteDBHelper db;
        public static LiteDBHelper LiteDB
        {
            get
            {
                if (db == null)
                {
                    string path = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    db = new LiteDBHelper(Path.Combine(path, "DairyDataDatabase.db"));
                    //var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DairyDataDatabase.db");
                    //db = new LiteDBHelper(path);
                }
                return db;
            }
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync($"{nameof(ExtendedNavigationPage)}/{nameof(LoginPage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ExtendedNavigationPage>();
            //containerRegistry.RegisterForNavigation<SharedTransitionNavigationPage>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<PlacePage, PlacePageViewModel>();
            containerRegistry.RegisterForNavigation<PlaceDetailPage, PlaceDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<CreatePlacePage, CreatePlacePageViewModel>();

        }

        public enum Theme { Light, Dark }
}
}
