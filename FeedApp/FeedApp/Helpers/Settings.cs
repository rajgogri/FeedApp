﻿using System;
using Xamarin.Essentials;

namespace FeedApp.Helpers
{
    public class Settings
    {
        public static string BaseUrl = "https://27.32.5.149:9884/api";

        public static string LoginUrl = "https://27.32.5.149:9884";

        static Settings _setting;
        public static Settings Instance
        {
            get
            {
                if (_setting == null)
                    _setting = new Settings();
                return _setting;
            }
        }
       

        public bool IsInternetAvailable()
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
                return true;
            else
                return false;
        }
    }
}
