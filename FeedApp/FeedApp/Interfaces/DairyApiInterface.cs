﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FeedApp.ResponseModel;
using Refit;

namespace FeedApp.Interfaces
{
    public interface DairyApiInterface
    {
        [Post("/token")]
        Task<LoginResponseModel> Login([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, object> data);

        [Post("/fileUpload/ImageUpload")]
        [Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")]
        Task<int> UploadImage([Header("Authorization")]string token,[Body] string userdata);

        [Post("/diaryData/addDiaryData")]
        [Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")]
        Task CreatePost([Header("Authorization")]string token,[Body] string postData);

        [Get("/diaryData/getallrecords/Neosofttech")]
        [Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")]
        Task<DataItems> GetAllPosts([Header("Authorization")]string token);
    }
}
