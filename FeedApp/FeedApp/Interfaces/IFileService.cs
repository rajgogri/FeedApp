﻿using System;
using System.IO;

namespace FeedApp.Interfaces
{
    public interface IFileService
    {
        //string SavePicture(string name, Stream data, string location = "temp");
        string SaveFile(string fileName, byte[] fileStream);
        void DeleteDirectory();
    }
}
