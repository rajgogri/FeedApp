﻿using System;
using System.Collections.Generic;
using FeedApp.Models;
using LiteDB;

namespace FeedApp.LiteDBManager
{
    public class LiteDBHelper
    {
        protected LiteCollection<Post> postCollection;

        public LiteDBHelper(string dbPath)
        {
            using (var db = new LiteDatabase(dbPath))
            {
                postCollection = db.GetCollection<Post>("Posts");
            }

        }

        public List<Post> GetAllPersons()
        {
            var persons = new List<Post>(postCollection.FindAll());
            return persons;
        }

        public void AddPost(Post post)
        {
            try
            {
                postCollection.Insert(post);
            }
            catch (Exception ex)
            {

            }
        }
        public void DeletePerson(int personId)
        {
            postCollection.Delete(a => a.Id == personId);
        }
    }
}
