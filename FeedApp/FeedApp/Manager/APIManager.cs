﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FeedApp.Helpers;
using FeedApp.Interfaces;
using FeedApp.ResponseModel;
using Newtonsoft.Json;
using Refit;

namespace FeedApp.Manager
{
    public class APIManager
    {
        static APIManager _manager;

        public APIManager()
        {
        }
        public static APIManager Instance
        {
            get
            {
                if (_manager == null)
                    _manager = new APIManager();
                return _manager;
            }
        }

        public async Task<LoginResponseModel> LoginAPI(Dictionary<string, object> userData)
        {
            try
            {
                var data = await RestService.For<DairyApiInterface>(Settings.LoginUrl).Login(userData);
                
                return data;
            }
            catch (Refit.ApiException ex)
            {
               return JsonConvert.DeserializeObject<LoginResponseModel>(ex.Content);
            }
            catch (Exception ex)
            {
                return new LoginResponseModel
                {
                    Error = ex.Message
                };
            }
        }

    }
}
