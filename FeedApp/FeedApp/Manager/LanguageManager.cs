﻿using System;
namespace FeedApp.Manager
{
    public class LanguageManager
    {
        static LanguageManager _manager;
        public static LanguageManager Instance
        {
            get
            {
                if (_manager == null)
                    _manager = new LanguageManager();

                return _manager;
            }
        }

        public void SetLanguage(string language)
        {
            Xamarin.Essentials.Preferences.Set("Lang", language);
        }

        public string GetLanguage()
        {
            var lang = Xamarin.Essentials.Preferences.Get("Lang", "English");
            return lang;
        }

        
    }
}
