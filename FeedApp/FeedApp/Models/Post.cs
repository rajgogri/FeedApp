﻿using System;
namespace FeedApp.Models
{
    public class Post
    {
        public int Id { get; set; } 
        public string Image { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Address { get; set; }
        public string Date { get; set; }
        public string CreatedTime { get; set; }
    }
}
