﻿using System;
using Newtonsoft.Json;

namespace FeedApp.RequestModel
{
    public class LoginRequestModel
    {
        [JsonProperty(PropertyName = "grant_type")]
        public string GrantType { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}
