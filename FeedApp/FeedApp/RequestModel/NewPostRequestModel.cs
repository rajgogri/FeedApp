﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FeedApp.RequestModel
{
    public class NewPostRequestModel
    {
        [JsonProperty(PropertyName = "recID")]
        public string RecID { get; set; }

        [JsonProperty(PropertyName = "CategoryID")]
        public string CategoryID { get; set; }

        [JsonProperty(PropertyName = "diaryCategory")]
        public string DiaryCategory { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "diaryDate")]
        public string DiaryDate { get; set; }

        [JsonProperty(PropertyName = "diaryTime")]
        public string DiaryTime { get; set; }

        [JsonProperty(PropertyName = "picturetempID")]
        public string PicturetempID { get; set; }

        [JsonProperty(PropertyName = "HasImage")]
        public string HasImage { get; set; }

        [JsonProperty(PropertyName = "FullAddress")]
        public string FullAddress { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "PostCode")]
        public string PostCode { get; set; }

        [JsonProperty(PropertyName = "Region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty(PropertyName = "StreetName")]
        public string StreetName { get; set; }

        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "diaryLat")]
        public string DiaryLat { get; set; }

        [JsonProperty(PropertyName = "diaryLng")]
        public string DiaryLng { get; set; }

    }
}
