﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FeedApp.ResponseModel
{
    public class DairyPostListResponseModel
    {
            [JsonProperty(PropertyName = "$id")]
            public string id { get; set; }

            [JsonProperty(PropertyName = "recID")]
            public string RecID { get; set; }

            [JsonProperty(PropertyName = "Title")]
            public string Title { get; set; }

            [JsonProperty(PropertyName = "Description")]
            public string Description { get; set; }

            [JsonProperty(PropertyName = "diaryDate")]
            public string DiaryDate { get; set; }

            [JsonProperty(PropertyName = "diaryTime")]
            public string DiaryTime { get; set; }

            [JsonProperty(PropertyName = "diaryImageUrl")]
            public string DiaryImageUrl { get; set; }

            [JsonProperty(PropertyName = "HasImage")]
            public bool HasImage { get; set; }

            [JsonProperty(PropertyName = "diaryCategory")]
            public string DiaryCategory { get; set; }

            [JsonProperty(PropertyName = "FullAddress")]
            public string FullAddress { get; set; }

        }

        public class DataItems
        {
            [JsonProperty(PropertyName = "$id")]
            public string DiaryLat { get; set; }

            [JsonProperty(PropertyName = "diaryItems")]
            public List<DairyPostListResponseModel> DiaryItemList { get; set; }
        }
}
