﻿using System;
using Xamarin.Forms;

namespace FeedApp.Utils
{
    public class CommonMethods
    {
        public CommonMethods()
        {
        }

        public static async void PermissionToOpenSetting()
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                var answer = await Application.Current.MainPage.DisplayAlert(AppResources.AppResources.Permission,
                    AppResources.AppResources.PrvsySetngPrvtng +
                    AppResources.AppResources.WntNvgt2PrvdPrmsn,
                    AppResources.AppResources.OK, AppResources.AppResources.CANCEL);
                if (answer)
                    DependencyService.Get<IMediaPermission>().AllowCameraPermission();
            }
        }

    }
}
