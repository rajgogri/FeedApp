﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FeedApp.Models;

namespace FeedApp.Utils
{
    public class Data
    {
        public static ObservableCollection<PostModel> places = new ObservableCollection<PostModel> {

            new PostModel
                {
                    Image = "https://www.fodors.com/wp-content/uploads/2019/12/WinterBeachEscapes__HERO_shutterstock_1271867908.jpg",
                    Title = "Ocean at Algarve",
                    SubTitle = "Enjoy view over the sky from your room",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    Category = "Category 1",
                    Address = "LBS Marg, Opp R-City Mall Between Ghatkopar & Vikhroli,lat:25.03428, long: 25.03428",
                    Date = "25/3/2020",
                    CreatedTime = "05:01:00 PM",
                },
                new PostModel
                {
                    Image = "https://i2.wp.com/ilc.oey.mybluehost.me/wp-content/uploads/2020/02/family_treehouse_dominican_village.jpg?resize=1024%2C576&ssl=1",
                    Title = "Dominican Tree House Village",
                    SubTitle = "Samana, Dominican Republic",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    Category = "Category 2",
                    Address = "LBS Marg, Opp R-City Mall Between Ghatkopar & Vikhroli,lat:25.03428, long: 25.03428",
                    Date = "25/3/2020",
                    CreatedTime = "05:01:00 PM"
                },
                new PostModel
                {
                    Image = "https://images.trvl-media.com/hotels/30000000/29850000/29842500/29842432/2debbc87.jpg?impolicy=fcrop&w=1200&h=800&p=1&q=medium",
                    Title = "Mandarin Oriental Jumeira",
                    SubTitle = "Dubai's Jumeirah",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    Category = "Category 3",
                    Address = "LBS Marg, Opp R-City Mall Between Ghatkopar & Vikhroli,lat:25.03428, long: 25.03428",
                    Date = "25/3/2020",
                    CreatedTime = "05:01:00 PM",
                },
                new PostModel
                {
                    Image = "https://images.trvl-media.com/hotels/21000000/20650000/20643300/20643202/b4d5f067.jpg?impolicy=fcrop&w=1200&h=800&p=1&q=medium",
                    Title = "Bulgari Hotel & Resorts",
                    SubTitle = "Dubai's Jumeirah",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    Category = "Category 4",
                    Address = "LBS Marg, Opp R-City Mall Between Ghatkopar & Vikhroli,lat:25.03428, long: 25.03428",
                    Date = "25/3/2020",
                    CreatedTime = "05:01:00 PM",
                },
                new PostModel
                {
                    Image = "https://cdn.galaxy.tf/thumb/sizeW1920/uploads/2s/cms_image/001/581/930/1581930687_5e4a58bf7be10-thumb.jpg",
                    Title = "Impressive Resorts & Spas",
                    SubTitle = "Bávaro, Punta Cana, Dominican Republic.",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    Category = "Category 5",
                    Address = "LBS Marg, Opp R-City Mall Between Ghatkopar & Vikhroli,lat:25.03428, long: 25.03428",
                    Date = "25/3/2020",
                    CreatedTime = "05:01:00 PM",
                },
                new PostModel
                {
                    Image = "https://media.cntraveler.com/photos/5d921b846168d9000af15c44/master/w_1200,c_limit/Tongabezi-&-Sindabezi_2019_IMG_0598-1.jpg",
                    Title = "Tongabezi & Sindabezi",
                    SubTitle = "Livingstone Way, Livingstone, Zambia.",
                    Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    Category = "Category 5",
                    Address = "LBS Marg, Opp R-City Mall Between Ghatkopar & Vikhroli,lat:25.03428, long: 25.03428",
                    Date = "25/3/2020",
                    CreatedTime = "05:01:00 PM",
                }
        };

    }
}
