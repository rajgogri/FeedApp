﻿namespace FeedApp.Utils
{
    internal interface IMediaPermission
    {
        void AllowCameraPermission();
    }
}