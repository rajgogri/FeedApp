﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using FeedApp.Interfaces;
using FeedApp.Models;
using Plugin.FilePicker;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.Toast;
using Prism.AppModel;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FeedApp.ViewModels
{
    public class CreatePlacePageViewModel: ViewModelBase, IAutoInitialize
    {
        Stream stream;
        Stream stream1;
        ImageSource imageSource;
        string base1;
        public PostModel Place { get; set; }
        public string Address1 { get; set; }
        INavigationService navigationPage;
        string message;
        string imagePath;
        Plugin.Media.Abstractions.MediaFile filePath;

        public CreatePlacePageViewModel(INavigationService navigationPage) : base(navigationPage)
        {
            this.navigationPage = navigationPage;
            DateTime localDate = DateTime.Now;
            //var b = string.Format("{0: hh:mm:ss tt}", localDate);
            try
            {
                Time = DateTime.Now.TimeOfDay;
                Date = localDate.Date;
            }
            catch (Exception ex)
            {
            }

        }

        public ICommand SaveButtonTapped
        {
            get
            {
                return new Command(async s =>
                {
                    if (string.IsNullOrEmpty(Title))
                    {
                        message = "Title field is empty";
                        CrossToastPopUp.Current.ShowToastMessage(message);
                    }
                    else if(string.IsNullOrEmpty(Category)){
                        message = "Category field is empty";
                        CrossToastPopUp.Current.ShowToastMessage(message);
                    }
                    else if(string.IsNullOrEmpty(Description))
                    {
                        message = "Description field is empty";
                        CrossToastPopUp.Current.ShowToastMessage(message);
                    }
                    else {


                        string date = Date.ToString("dd/MM/yyyy");
                        var time = Time.ToString("T");
                        var post1 = new Post
                        {
                            Description = Description,
                            Title = Title,
                            SubTitle = "Description field is empty",
                            Category = Category,
                            Date = date,
                            CreatedTime = time,
                            Address = Address1,
                            Image = imagePath
                        };
                        App.LiteDB.AddPost(post1);
                        await this.navigationPage.NavigateAsync("PlacePage");
                        message = "Saved data successfully";
                    }
                });
            }
        }

        public ICommand ProfileImageTapped
        {
            get
            {
                return new Command(async s =>
                {
                    Dictionary<string, string> trackProg = new Dictionary<string, string>();
                    try
                    {
                        trackProg.Add("Checking permission for picking photo : ", "Yes");
                        
                        var action = await Application.Current.MainPage.DisplayActionSheet("Add Photo", "Cancel", null, "Choose Existing", "Take Photo");
                        var galleryStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Photos);
                        var mediaStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.MediaLibrary);
                        var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                        if (storageStatus == Plugin.Permissions.Abstractions.PermissionStatus.Denied)
                        {
                            var response = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                        }

                        if (action == "Choose Existing")
                        {
                            try
                            {
                                trackProg.Add("Inside Choose Existing : ", "Yes");
                                MainThread.BeginInvokeOnMainThread(async () =>
                                {
                                    await CrossMedia.Current.Initialize();

                                    Plugin.Media.Abstractions.MediaFile file = null;

                                    try
                                    {
                                        if (Device.RuntimePlatform == Device.Android)
                                        {
                                            file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                                            {
                                                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                                            });
                                        }
                                        else
                                        {

                                            file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                                            {
                                                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                                                SaveMetaData = false,
                                                RotateImage = false
                                            });
                                        }
                                        trackProg.Add("Image picked form gallery : ", "Yes");
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                    if (file == null)
                                        return;

                                    if (file.Path != null)
                                    {
                                        filePath = file;
                                        imagePath = file.Path;
                                        Image = ImageSource.FromFile(file.Path);
                                        //SaveImage(file.Path);
                                        stream1 = file.GetStream();
                                        //file.Dispose();
                                    }

                                });
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else if (action == "Take Photo")
                        {
                            try
                            {
                                trackProg.Add("Take Photo from camera: ", "Yes");

                                await CrossMedia.Current.Initialize();                               
                                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                                if (cameraStatus == Plugin.Permissions.Abstractions.PermissionStatus.Denied)
                                {
                                    //Settings.PermissionToOpenSetting();
                                }
                                Plugin.Media.Abstractions.MediaFile file = null;

                                try
                                {
                                    if (Device.RuntimePlatform == Device.Android)
                                    {

                                        file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                                        {
                                            Directory = "Test",
                                            SaveToAlbum = true,
                                            CompressionQuality = 75,
                                            CustomPhotoSize = 50,
                                            PhotoSize = PhotoSize.MaxWidthHeight,
                                            MaxWidthHeight = 2000,
                                            DefaultCamera = CameraDevice.Front
                                        });
                                    }
                                    else
                                    {
                                        file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                                        {
                                            Directory = "Test",
                                            SaveToAlbum = true,
                                            CompressionQuality = 75,
                                            CustomPhotoSize = 50,
                                            PhotoSize = PhotoSize.MaxWidthHeight,
                                            MaxWidthHeight = 2000,
                                            RotateImage = false,
                                            SaveMetaData = false,
                                            DefaultCamera = CameraDevice.Front
                                        });
                                    }
                                    trackProg.Add("Photo from camera: ", "Yes");
                                }
                                catch (Exception ex)
                                {
                                    //Crashes.TrackError(ex, trackProg);
                                }
                                if (file == null)
                                    return;

                                if (file.Path != null)
                                {
                                    filePath = file;
                                    imagePath = file.Path;
                                    Image = ImageSource.FromFile(file.Path);
                                    stream1 = file.GetStream();
                                    file.Dispose();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Crashes.TrackError(ex, trackProg);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //Crashes.TrackError(ex, trackProg);
                    }
                });
            }
        }

        public ICommand TimePicker_PropertyChanged
        {
            get
            {
                return new Command(async s =>
                {
                    
                });
            }
        }

        private ImageSource image = "PlaceholderImg.jpeg";
        public ImageSource Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                RaisePropertyChanged("Image");
            }
        }

        private string title;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                RaisePropertyChanged("Title");
            }
        }
        private string category;
        public string Category
        {
            get
            {
                return category;
            }
            set
            {
                category = value;
                RaisePropertyChanged("Category");
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                RaisePropertyChanged("Description");
            }
        }

        private TimeSpan time;
        public TimeSpan Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
                RaisePropertyChanged("Time");
            }
        }
        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
                RaisePropertyChanged("Date");
            }
        }
    }
}
