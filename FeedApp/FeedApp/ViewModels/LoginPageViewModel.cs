﻿using System;
using Prism.Mvvm;
using Prism.AppModel;
using Prism.Navigation;
using System.Windows.Input;
using Xamarin.Forms;
using FeedApp.Controls;
using FeedApp.Views;
using Plugin.Toast;
using Plugin.SharedTransitions;
using Xamarin.Essentials;
using FeedApp.Helpers;
using FeedApp.Manager;
using FeedApp.RequestModel;
using System.Collections.Generic;

namespace FeedApp.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private string message = "";
        INavigationService navigationPage;
        public LoginPageViewModel(INavigationService navigationPage):base(navigationPage)
        {
           this.navigationPage = navigationPage;
        }
        public ICommand OnLoginNowTapped
        {
            get
            {
                return new Command(async s =>
                {
                    if (string.IsNullOrEmpty(UserName))
                    {
                        message = "Username field is empty";
                        CrossToastPopUp.Current.ShowToastMessage(message);
                    }
                    else if (string.IsNullOrEmpty(Password))
                    {
                        message = "Password field is empty";
                        CrossToastPopUp.Current.ShowToastMessage(message);
                    }
                    else
                    {
                        if (Settings.Instance.IsInternetAvailable())
                        {
                            //LoginRequestModel loginRequestModel = new LoginRequestModel();
                            //loginRequestModel.UserName = UserName;
                            //loginRequestModel.Password = Password;
                            //loginRequestModel.GrantType = "password";
                            Dictionary<string, object> data = new Dictionary<string, object>() {
                                { "grant_type", "password" },
                                { "username", UserName },
                                { "password", Password },
                            };

                            var result = await APIManager.Instance.LoginAPI(data);
                            if (result != null)
                                if (result.Error == null)
                                {
                                    Preferences.Set("token", result.TokenType + result.AccessToken);
                                    await navigationPage.NavigateAsync("PlacePage");
                                    message = "Login Successful";
                                    CrossToastPopUp.Current.ShowToastMessage(message);
                                }
                                else {
                                    message = string.IsNullOrEmpty(result.ErrorDescription) ? result.Error : result.ErrorDescription;
                                    CrossToastPopUp.Current.ShowToastMessage(message);
                                }
                        }
                        else
                        {
                            message = "No Internet";
                            CrossToastPopUp.Current.ShowToastMessage(message);
                        }
                    }
                });
            }
        }
        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                RaisePropertyChanged("UserName");
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                RaisePropertyChanged("Password");
            }
        }
    }
}