﻿using System;
using System.Windows.Input;
using FeedApp.Models;
using Prism.AppModel;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FeedApp.ViewModels
{
    public class PlaceDetailPageViewModel : ViewModelBase, IAutoInitialize
    {
        public PostModel post { get; set; }
        INavigationService navigationPage;
        public PlaceDetailPageViewModel(INavigationService navigationPage) : base(navigationPage)
        {
            this.navigationPage = navigationPage;
        }

        public ICommand BackButtonTapped
        {
            get
            {
                return new Command(async s =>
                {
                    try
                    {
                        var a = await this.navigationPage.NavigateAsync("PlacePage");
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
        }
    }
}
