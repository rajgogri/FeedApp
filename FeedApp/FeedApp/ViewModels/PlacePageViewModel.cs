﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using FeedApp.Controls;
using FeedApp.Models;
using FeedApp.Utils;
using FeedApp.Views;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FeedApp.ViewModels
{
    public class PlacePageViewModel : ViewModelBase
    {
        public ObservableCollection<PostModel> Items { get; set; }
        public ObservableCollection<Models.Tab> TabItems { get; set; }
        public Models.Tab Item { get; set; }
        public string SelectedItemId { get; set; }
        public ListLayoutOptions ListLayout { get; set; }
        public string Section { get; set; }
        public DelegateCommand<PostModel> GoToDetailCommand { get; set; }
        public DelegateCommand<object> ChangeLayoutCommand { get; set; }
        public DelegateCommand AddButtonTapped { get; set; }
        private INavigationService navigationPage;
        private string Address1;

        public PlacePageViewModel(INavigationService navigationPage) : base(navigationPage)
        {
            this.navigationPage = navigationPage;
            GoToDetailCommand = new DelegateCommand<PostModel>(GoToDetail);
            Items = new ObservableCollection<PostModel>();
            Item = new Models.Tab();
            ListLayout = ListLayoutOptions.Big;
            AddButtonTapped = new DelegateCommand(AddButtonTapped1);
            ChangeLayoutCommand = new DelegateCommand<object>(ChangeLayout);
            //LoadData();
        }
        void ChangeLayout(object listLayout)
        {
            ListLayout = (ListLayoutOptions)listLayout;
        }
        void GoToDetail(PostModel post)
        {
            SelectedItemId = post.Id.ToString();
            var navParam = new NavigationParameters { { nameof(post), post } };
            MainThread.BeginInvokeOnMainThread(async () => {
                await this.navigationPage.NavigateAsync("PlaceDetailPage", navParam);
            });
            //await NavigationService.NavigateAsync($"{nameof(ExtendedNavigationPage)}/{nameof(PlaceDetailPage)}",navParam);

        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() != Prism.Navigation.NavigationMode.Back)
            {
               LoadData();
            }
            else
            {
                SelectedItemId = null;
            }
        }
        void LoadData()
        {
            var all = new List<PostModel>();
            var postList = App.LiteDB.GetAllPersons();
            string localPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            for (int i = 0; i < postList.Count; i++)
            {
                var element = postList[i];
                all.Add(new PostModel
                {
                    Id = element.Id,
                    Description = element.Description,
                    Title = element.Title,
                    SubTitle = element.SubTitle,
                    Category = element.Category,
                    Date = element.Date,
                    CreatedTime = element.CreatedTime,
                    Address = element.Address,
                    Image = element.Image,
                }) ;

            }
            Items = new ObservableCollection<PostModel>(all);

        }
        async void AddButtonTapped1()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                var location = await Geolocation.GetLocationAsync(request);
                var lat = location.Latitude;
                var lon = location.Longitude;

                var placemarks = await Geocoding.GetPlacemarksAsync(lat, lon);

                var placemark = placemarks?.FirstOrDefault();
                Address1 = placemark.FeatureName + ", "+ placemark.SubLocality + ", " + placemark.Locality + ", " + placemark.SubAdminArea + ", " + placemark.PostalCode + ", " + placemark.CountryName;
                if (location != null)
                {
                    Console.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to get location
            }

            var navParam = new NavigationParameters { { nameof(Address1), Address1 } };

            await this.navigationPage.NavigateAsync("CreatePlacePage",navParam);
        }


    }

}
