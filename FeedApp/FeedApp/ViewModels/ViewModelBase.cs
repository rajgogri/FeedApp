﻿using System;
using System.ComponentModel;
using Prism.Mvvm;
using Prism.Navigation;

namespace FeedApp.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible, INotifyPropertyChanged
    {
        protected INavigationService NavigationService { get; private set; }



        public ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        private async void OnGoHomeCommandExecuted()
        {
            var result = await NavigationService.NavigateAsync("PlacePage");
            if (!result.Success)
            {
                System.Diagnostics.Debugger.Break();
            }
        }
        internal void OnNavigatedTo()
        {
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatingTo(INavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }
    }
}
