﻿using System;
using System.Collections.Generic;
using FeedApp.ViewModels;
using Xamarin.Forms;

namespace FeedApp.Views
{
    public partial class CreatePlacePage : ContentPage
    {
        public CreatePlacePage()
        {
            NavigationPage.SetHasBackButton(this, false);

            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            detailContainer.FadeTo(1, 200, Easing.CubicInOut);
            detailContainer.TranslateTo(0, 0, 200, Easing.CubicInOut);
        }

        private void EntryTitle_Focused(object sender, FocusEventArgs e)
        {
            titleBoxView.BackgroundColor = Color.Red;
        }

        private void EntryTitle_UnFocused(object sender, FocusEventArgs e)
        {
            titleBoxView.BackgroundColor = Color.Gray;
        }

        private void EntryDecription_Focused(object sender, FocusEventArgs e)
        {
            descriptionBoxView.BackgroundColor = Color.Red;
        }

        private void EntryDecription_Unfocused(object sender, FocusEventArgs e)
        {
            descriptionBoxView.BackgroundColor = Color.Gray;
        }

        private void EntryCategory_Focused(object sender, FocusEventArgs e)
        {
            categoryBoxView.BackgroundColor = Color.Red;
        }

        private void EntryCategory_UnFocused(object sender, FocusEventArgs e)
        {
            categoryBoxView.BackgroundColor = Color.Gray;
        }
        private void EntryDate_Focused(object sender, FocusEventArgs e)
        {
            dateBoxView.BackgroundColor = Color.Red;
        }

        private void EntryDate_UnFocused(object sender, FocusEventArgs e)
        {
            dateBoxView.BackgroundColor = Color.Black;
        }

        private void EntryTime_Focused(object sender, FocusEventArgs e)
        {
            TimeBoxView.BackgroundColor = Color.Red;
        }

        private void EntryTime_UnFocused(object sender, FocusEventArgs e)
        {
            TimeBoxView.BackgroundColor = Color.Black;
        }

       
    }
}
