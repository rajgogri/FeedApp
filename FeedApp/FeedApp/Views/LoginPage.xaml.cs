﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FeedApp.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        private void EntryPassword_Focused(object sender, FocusEventArgs e)
        {
            PasswordLbl.TextColor = Color.Red;
            Passwordboxview.BackgroundColor = Color.Red;

        }

        private void EntryPassword_Unfocused(object sender, FocusEventArgs e)
        {
            PasswordLbl.TextColor = Color.Black;
            Passwordboxview.BackgroundColor = Color.Black;
        }

        private void EntryUserName_Focused(object sender, FocusEventArgs e)
        {
            UsernameLbl.TextColor = Color.Red;
            Usernameboxview.BackgroundColor = Color.Red;
        }

        private void EntryUserName_Unfocused(object sender, FocusEventArgs e)
        {
            UsernameLbl.TextColor = Color.Black;
            Usernameboxview.BackgroundColor = Color.Black;

        }
    }
}
