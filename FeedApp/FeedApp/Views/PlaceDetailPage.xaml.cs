﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace FeedApp.Views
{
    public partial class PlaceDetailPage : ContentPage
    {
        public PlaceDetailPage()
        {
            NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
            

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            detailContainer.FadeTo(1, 200, Easing.CubicInOut);
            detailContainer.TranslateTo(0, 0, 200, Easing.CubicInOut);
        }



    }
}
