﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace FeedApp.Views
{
    public partial class PlacePage : ContentPage
    {
        public PlacePage()
        {
            Xamarin.Forms.NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            var safeInsets = On<iOS>().SafeAreaInsets();
            safeInsets.Bottom = 0;
            if (safeInsets.Top == 0)
            {
                safeInsets.Top = 65;
                Padding = safeInsets;
            }
            else
            {
                Padding = safeInsets;
            }

        }
    }
}
